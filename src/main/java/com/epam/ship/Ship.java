package com.epam.ship;

import com.epam.collections.MyPriorityQueue;
import com.epam.ship.droid.Droid;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.function.Predicate;

public class Ship<T extends Droid> {

    private Queue<T> loads;

    public Ship() {
        loads = new MyPriorityQueue<>();
    }

    public void addCargo(T cargo) {
        loads.add(cargo);
    }

    public void addLoads(List<? extends T> loads) {
        this.loads.addAll(loads);
    }

    public Queue<T> getLoads() {
        return loads;
    }

    public List<T> getLoadsIf(Predicate<? super T> p) {
        List<T> retLoads = new ArrayList<>(loads.size());
        for (T cargo : loads) {
            if (p.test(cargo)) {
                retLoads.add(cargo);
            }
        }
        return retLoads;
    }

    public void copyLoads(List<? super T> dest) {
        dest.addAll(loads);
    }


}
