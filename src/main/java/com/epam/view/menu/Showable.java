package com.epam.view.menu;

import com.epam.view.Runner;

import java.util.Map;

public interface Showable {
    Map<String, String> getMenu();

    Map<String, Runner> getRunMenu();
}
