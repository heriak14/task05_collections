package com.epam.view.menu;

import com.epam.ship.Ship;
import com.epam.ship.droid.Droid;
import com.epam.ship.droid.DroidHealer;
import com.epam.view.Runner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ShipMenu implements Showable {
    private static final Logger LOG = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in, "UTF-8");
    private static final Random RANDOM = new Random();
    private Ship<Droid> ship;
    private Map<String, String> menu;
    private Map<String, Runner> runMenu;

    public ShipMenu() {
        ship = new Ship<>();
        menu = new LinkedHashMap<>();
        menu.put("1", "LOAD SHIP WITH DROIDS");
        menu.put("2", "PRINT DROIDS FROM SHIP");
        menu.put("3", "UNLOAD DROIDS FROM SHIP");
        menu.put("Q", "QUIT");

        runMenu = new LinkedHashMap<>(menu.size());
        runMenu.put("1", this::loadShip);
        runMenu.put("2", this::printDroids);
        runMenu.put("3", this::unloadShip);
    }

    public Map<String, String> getMenu() {
        return menu;
    }

    public Map<String, Runner> getRunMenu() {
        return runMenu;
    }

    private void loadShip() {
        LOG.trace("Enter number of droids you want to load: ");
        int number = SCANNER.nextInt();
        for (int i = 0; i < number; i++) {
            if (RANDOM.nextInt(i+1) > 0) {
                ship.addCargo(new Droid());
            } else {
                ship.addCargo(new DroidHealer());
            }
        }
    }

    private void unloadShip() {
        LOG.trace("Enter number of droids you want to load: ");
        int number = SCANNER.nextInt();
        if (number > ship.getLoads().size()) {
            number = ship.getLoads().size();
        }
        for (int i = 0; i < number; i++) {
            LOG.trace(ship.getLoads().poll() + " was unloaded!\n");
        }
    }

    private void printDroids() {
        for (Droid d : ship.getLoads()) {
            LOG.trace(d + "\n");
        }
    }

}
