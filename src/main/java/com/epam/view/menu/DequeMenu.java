package com.epam.view.menu;

import com.epam.collections.MyDeque;
import com.epam.view.Runner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class DequeMenu implements Showable {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in, "UTF-8");
    private Deque<String> deque;
    private Map<String, String> menu;
    private Map<String, Runner> runMenu;

    public DequeMenu() {
        deque = new MyDeque<>();
        menu = new LinkedHashMap<>();
        menu.put("1", "Add element at the start");
        menu.put("2", "Add element at the end");
        menu.put("3", "Delete from the start");
        menu.put("4", "Delete from the end");
        menu.put("5", "Print");
        menu.put("6", "Print in reverse order");
        menu.put("Q", "Quit");

        runMenu = new LinkedHashMap<>();
        runMenu.put("1", this::addFirst);
        runMenu.put("2", this::addLast);
        runMenu.put("3", this::removeFirst);
        runMenu.put("4", this::removeLast);
        runMenu.put("5", this::print);
        runMenu.put("6", this::printReverse);
    }

    @Override
    public Map<String, String> getMenu() {
        return menu;
    }

    @Override
    public Map<String, Runner> getRunMenu() {
        return runMenu;
    }

    private void addFirst() {
        LOGGER.trace("Enter value you want to put in the start of deque: ");
        deque.addFirst(SCANNER.nextLine());
    }

    private void addLast() {
        LOGGER.trace("Enter value you want to put in the end of deque: ");
        deque.addLast(SCANNER.nextLine());
    }

    private void removeFirst() {
        try {
            LOGGER.trace("You've removed value: " + deque.removeFirst() + "\n");
        } catch (NoSuchElementException e) {
            LOGGER.error(e.getMessage() + "\n");
        }
    }

    private void removeLast() {
        try {
            LOGGER.trace("You've removed value: " + deque.removeLast());
        } catch (NoSuchElementException e) {
            LOGGER.error(e.getMessage() + "\n");
        }
    }

    private void print() {
        for (String e : deque) {
            LOGGER.trace(e + " ");
        }
        LOGGER.trace("\n");
    }

    private void printReverse() {
        Iterator it = deque.descendingIterator();
        while (it.hasNext()) {
            LOGGER.trace(it.next() + " ");
        }
        LOGGER.trace("\n");
    }
}
