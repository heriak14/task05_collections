package com.epam.view.menu;

import com.epam.collections.BinaryTreeMap;
import com.epam.view.Runner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class TreeMapMenu implements Showable {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in, "UTF-8");
    private Map<Integer, String> treeMap;
    private Map<String, String> menu;
    private Map<String, Runner> runMenu;

    public TreeMapMenu() {
        treeMap = new BinaryTreeMap<>();

        menu = new LinkedHashMap<>();
        menu.put("1", "Add to the map");
        menu.put("2", "Delete from map");
        menu.put("3", "Get value by key");
        menu.put("4", "Print");
        menu.put("Q", "Quit");

        runMenu = new LinkedHashMap<>();
        runMenu.put("1", this::put);
        runMenu.put("2", this::remove);
        runMenu.put("3", this::getValue);
        runMenu.put("4", this::printMap);
    }

    @Override
    public Map<String, String> getMenu() {
        return menu;
    }

    @Override
    public Map<String, Runner> getRunMenu() {
        return runMenu;
    }

    private void put() {
        while (true) {
            LOGGER.trace("Enter values(Integer - String): ");
            String[] input = SCANNER.nextLine().split("[ -]");
            if (input.length != 2) {
                LOGGER.trace("Wrong input! Try again...\n");
            } else {
                try {
                    Integer key = Integer.parseInt(input[0].trim());
                    String value = input[1].trim();
                    treeMap.put(key, value);
                    break;
                } catch (NumberFormatException e) {
                    LOGGER.trace("Wrong input! Try again...\n");
                }
            }
        }
    }

    private void remove() {
        LOGGER.trace("Enter key to delete value: ");
        Integer key = Integer.parseInt(SCANNER.nextLine());
        treeMap.remove(key);
    }

    private void getValue() {
        LOGGER.trace("Enter key to get value from tree map: ");
        Integer key = Integer.parseInt(SCANNER.nextLine());
        String value = treeMap.get(key);
        LOGGER.trace("Your value " + (Objects.isNull(value) ? "doesn't exist." : ("= " + value)) + "\n");
    }

    private void printMap() {
        LOGGER.trace("Your tree map:\n");
        for (int key : treeMap.keySet()) {
            LOGGER.trace(key + " - " + treeMap.get(key) + "\n");
        }
    }
}
