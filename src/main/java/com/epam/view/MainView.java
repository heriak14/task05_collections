package com.epam.view;

import com.epam.container.ContainerTester;
import com.epam.view.menu.DequeMenu;
import com.epam.view.menu.ShipMenu;
import com.epam.view.menu.Showable;
import com.epam.view.menu.TreeMapMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {
    private static final Logger LOG = LogManager.getLogger();
    private static final Scanner SCAN = new Scanner(System.in, "UTF-8");
    private Map<String, String> menu;
    private Map<String, Runner> runMenu;
    private Showable showable;

    public MainView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "DROID LOADER");
        menu.put("2", "TEST MY DEQUE");
        menu.put("3", "TEST CUSTOM BINARY TREE MAP");
        menu.put("4", "TEST CUSTOM CONTAINERS");
        menu.put("Q", "QUIT");

        runMenu = new LinkedHashMap<>();
        runMenu.put("1", this::startShipView);
        runMenu.put("2", this::testDeque);
        runMenu.put("3", this::testBinaryTreeMap);
        runMenu.put("4", this::testContainers);
    }

    private void showMenu(Map<String, String> menu) {
        LOG.trace("------------MENU------------\n");
        for (String key : menu.keySet()) {
            LOG.trace(key + " - " + menu.get(key) + "\n");
        }
    }

    private void startShipView() {
        showable = new ShipMenu();
        this.show(showable.getMenu(), showable.getRunMenu());
    }

    private void testDeque() {
        showable = new DequeMenu();
        this.show(showable.getMenu(), showable.getRunMenu());
    }

    private void testBinaryTreeMap() {
        showable = new TreeMapMenu();
        this.show(showable.getMenu(), showable.getRunMenu());
    }

    private void testContainers() {
        LOG.trace("\n");
        ContainerTester.testStringArray();
        LOG.trace("\n");
        ContainerTester.testCountry();
    }

    private void show(Map<String, String> menu, Map<String, Runner> runMenu) {
        String key;
        do {
            showMenu(menu);
            LOG.trace("Enter your choice: ");
            key = SCAN.nextLine().toLowerCase();
            if (runMenu.containsKey(key)) {
                runMenu.get(key).run();
            } else if (!key.equals("q")) {
                LOG.trace("Wrong input!\n");
            }
        } while (!key.equals("q"));
    }

    public void show() {
        this.show(menu, runMenu);
    }
}
