package com.epam.container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ContainerTester {

    private static final Logger LOG = LogManager.getLogger();
    private static final int TESTS_NUMBER = 100;

    public static void testCountry() {
        Country[] countryArray = {new Country("Ukraine", "Kiev"),
                new Country("Poland", "Warsaw"), new Country("USA", "Washington"),
                new Country("England", "London")};
        List<Country> countryList  = Arrays.asList(countryArray);
        LOG.trace("Countries before sorting:\n");
        for (Country c : countryArray) {
            LOG.trace(c + "\n");
        }
        Arrays.sort(countryArray);
        LOG.trace("\nCountries after sorting by country name:\n");
        for (Country c : countryArray) {
            LOG.trace(c + "\n");
        }
        Collections.sort(countryList, Comparator.comparing(Country::getCapital));
        LOG.trace("\nCountries after sorting by capital:\n");
        for (Country c : countryList) {
            LOG.trace(c + "\n");
        }
    }

    public static void testStringArray() {
        StringArray stringArray = new StringArray();
        List<String> stringList = new ArrayList<>();
        long startTime = System.nanoTime();
        for (int i = 0; i < TESTS_NUMBER; i++) {
            stringArray.add(i + "");
        }
        long endTime = System.nanoTime();
        LOG.trace("StringArray time to add " + TESTS_NUMBER + " elements = " + (endTime - startTime) + "\n");

        startTime = System.nanoTime();
        for (int i = 0; i < TESTS_NUMBER; i++) {
            stringList.add(i + "");
        }
        endTime = System.nanoTime();
        LOG.trace("ArrayList time to add " + TESTS_NUMBER + " elements = " + (endTime - startTime) + "\n");
    }
}
