package com.epam.container;

import java.util.Objects;

public class StringArray {

    private static final int START_CAPACITY = 10;
    private static final double INCREASING_СOEF = 1.5;
    private String[] array;

    public StringArray(int capacity) {
        array = new String[capacity];
    }

    public StringArray() {
        array = new String[START_CAPACITY];
    }

    public void add(String str) {
        for (int i = 0; i < array.length; i++) {
            if (Objects.isNull(array[i])) {
                array[i] = str;
                return;
            }
        }
        int newCapacity = (int) (array.length * INCREASING_СOEF + 1);
        String[] tmp = new String[newCapacity];
        copy(tmp, array);
        tmp[array.length] = str;
        array = tmp;
    }

    public String get(int index) {
        if (index > array.length) {
            return null;
        } else {
            return array[index];
        }
    }

    private void copy(String[] dest, String[] src) {
        for (int i = 0; i < src.length; i++) {
            dest[i] = src[i];
        }
    }
}
