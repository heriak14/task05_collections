package com.epam.container;

public class Country implements Comparable<Country> {
    private static final int ARRAY_SIZE = 10;
    private String country;
    private String capital;

    public Country(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCapital() {
        return capital;
    }

    @Override
    public String toString() {
        return country + " - " + capital;
    }

    @Override
    public int compareTo(Country o) {
        return this.country.compareTo(o.country);
    }
}
