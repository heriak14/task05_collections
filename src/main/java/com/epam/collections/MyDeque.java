package com.epam.collections;

import java.util.*;

public class MyDeque<E> implements Deque<E> {

    private class DequeNode<E> {
        private E element;
        private DequeNode<E> next;
        private DequeNode<E> prev;

        private DequeNode(E element) {
            this.element = element;
        }
    }

    private DequeNode<E> head;
    private DequeNode<E> tail;

    @Override
    public void addFirst(E e) {
        if (Objects.isNull(e)) {
            throw new IllegalArgumentException("MyDequeue does not permit null elements!");
        }
        if (Objects.isNull(head)) {
            head = new DequeNode<>(e);
            tail = head;
            return;
        }
        DequeNode<E> node = new DequeNode<>(e);
        node.next = head;
        head.prev = node;
        head = node;
    }

    @Override
    public void addLast(E e) {
        if (Objects.isNull(e)) {
            throw new IllegalArgumentException("MyDequeue does not permit null elements!");
        }
        if (Objects.isNull(head)) {
            head = new DequeNode<>(e);
            tail = head;
            return;
        }
        tail.next = new DequeNode<>(e);
        tail.next.prev = tail;
        tail = tail.next;
    }

    @Override
    public boolean offerFirst(E e) {
        try {
            addFirst(e);
            return true;
        } catch (IllegalArgumentException ex) {
            return false;
        }
    }

    @Override
    public boolean offerLast(E e) {
        try {
            addLast(e);
            return true;
        } catch (IllegalArgumentException ex) {
            return false;
        }
    }

    @Override
    public E removeFirst() {
        if (Objects.isNull(head)) {
            throw new NoSuchElementException("MyDeque is empty");
        }
        DequeNode<E> first = head;
        head = head.next;
        return first.element;
    }

    @Override
    public E removeLast() {
        if (Objects.isNull(head)) {
            throw new NoSuchElementException("MyDeque is empty");
        }
        DequeNode<E> last = tail;
        tail = tail.prev;
        tail.next = null;
        return last.element;
    }

    @Override
    public E pollFirst() {
        try {
            return removeFirst();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public E pollLast() {
        try {
            return removeLast();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public E getFirst() {
        if (Objects.isNull(head)) {
            throw new NoSuchElementException("MyDeque is empty");
        }
        return head.element;
    }

    @Override
    public E getLast() {
        if (Objects.isNull(tail)) {
            throw new NoSuchElementException("MyDeque is empty");
        }
        return tail.element;
    }

    @Override
    public E peekFirst() {
        try {
            return getFirst();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public E peekLast() {
        try {
            return getFirst();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (Objects.isNull(o) || Objects.isNull(head)) {
            return false;
        }
        if (head.element.equals(o)) {
            head = head.next;
            return true;
        }
        DequeNode<E> ptr = head;
        while (!Objects.isNull(ptr.next)) {
            if (ptr.next.element.equals(o)) {
                ptr.next = ptr.next.next;
                if (!Objects.isNull(ptr.next)) {
                    ptr.next.prev = ptr;
                }
                return true;
            } else {
                ptr = ptr.next;
            }
        }
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        if (Objects.isNull(o) || Objects.isNull(tail)) {
            return false;
        }
        if (tail.element.equals(o)) {
            tail = tail.prev;
            tail.next = null;
            return true;
        }
        DequeNode<E> ptr = tail;
        while (!Objects.isNull(tail.prev)) {
            if (ptr.prev.element.equals(o)) {
                ptr.prev = ptr.prev.prev;
                if (!Objects.isNull(ptr.prev)) {
                    ptr.prev.next = ptr;
                }
                return true;
            } else {
                ptr = ptr.prev;
            }
        }
        return false;
    }

    @Override
    public boolean add(E e) {
        addLast(e);
        return true;
    }

    @Override
    public boolean offer(E e) {
        try {
            return add(e);
        } catch (NullPointerException ex) {
            return false;
        }
    }

    @Override
    public E remove() {
        return removeFirst();
    }

    @Override
    public E poll() {
        return pollFirst();
    }

    @Override
    public E element() {
        return getFirst();
    }

    @Override
    public E peek() {
        return peekFirst();
    }

    @Override
    public void push(E e) {
        addFirst(e);
    }

    @Override
    public E pop() {
        return removeFirst();
    }

    @Override
    public boolean remove(Object o) {
        return removeFirstOccurrence(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        long size = c.stream().filter(e -> !this.contains(e)).count();
        return (size == 0);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        Iterator<? extends E> it = c.iterator();
        while (it.hasNext()) {
            if (!this.add(it.next())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        if (!this.containsAll(c)) {
            return false;
        }
        Iterator<?> it = c.iterator();
        while (it.hasNext()) {
            this.remove(it.next());
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Iterator<E> it = this.iterator();
        while (it.hasNext()) {
            if (!c.contains(it.next())) {
                this.remove(it.next());
            }
        }
        return true;
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (Objects.isNull(o)) {
            return false;
        } else if (this.getClass() != o.getClass()) {
            return false;
        }
        Deque<E> other = (Deque<E>) o;
        return this.containsAll(other);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for (E e : this) {
            hash += e.hashCode();
        }
        return hash;
    }

    @Override
    public boolean contains(Object o) {
        DequeNode<E> ptr = head;
        while (!Objects.isNull(ptr)) {
            if (ptr.element.equals(o)) {
                return true;
            } else {
                ptr = ptr.next;
            }
        }
        return false;
    }

    @Override
    public int size() {
        int size = 0;
        DequeNode<E> ptr = head;
        while (!Objects.isNull(ptr)) {
            size++;
            ptr = ptr.next;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return Objects.isNull(head);
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private DequeNode<E> node = head;

            @Override
            public boolean hasNext() {
                return !Objects.isNull(node);
            }

            @Override
            public E next() {
                DequeNode<E> tmp = node;
                node = node.next;
                return tmp.element;
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] objects = new Object[this.size()];
        int i = 0;
        for (E e : this) {
            objects[i] = e;
            i++;
        }
        return objects;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        int i = 0;
        Iterator<E> it = this.iterator();
        while (it.hasNext() && i < a.length) {
            a[i] = (T) it.next();
            i++;
        }
        return a;
    }

    @Override
    public Iterator<E> descendingIterator() {
        return new Iterator<E>() {
            @Override
            public boolean hasNext() {
                return !Objects.isNull(tail);
            }

            @Override
            public E next() {
                DequeNode<E> tmp = tail;
                tail = tail.prev;
                return tmp.element;
            }
        };
    }
}
