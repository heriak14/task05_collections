package com.epam.collections;

import java.util.*;

public class BinaryTreeMap<K, V> implements Map<K, V> {

    private Entry<K, V> root;
    private Comparator<K> comparator;
    private int size;

    public BinaryTreeMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    public BinaryTreeMap() {
    }

    private static final class Entry<K, V> implements Map.Entry<K, V> {

        private K key;
        private V value;
        private Entry<K, V> right;
        private Entry<K, V> left;

        private Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return this.key;
        }

        @Override
        public V getValue() {
            return this.value;
        }

        @Override
        public V setValue(V value) {
            this.value = value;
            return value;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return Objects.isNull(root);
    }

    @Override
    public boolean containsKey(Object key) {
        if (Objects.isNull(key)) {
            return false;
        }
        return containsKeyRec(key, root);
    }

    private boolean containsKeyRec(Object key, Entry<K, V> root) {
        if (Objects.isNull(root)) {
            return false;
        }
        if (root.key.equals(key)) {
            return true;
        }
        if (this.compareKeys((K) key, root.key) > 0) {
            return containsKeyRec(key, root.right);
        } else {
            return containsKeyRec(key, root.left);
        }
    }

    @Override
    public boolean containsValue(Object value) {
        return this.values().contains(value);
    }

    @Override
    public V get(Object key) {
        if (Objects.isNull(key)) {
            return null;
        }
        return getUsingRec(key, root);
    }

    private V getUsingRec(Object key, Entry<K, V> root) {
        if (Objects.isNull(root)) {
            return null;
        }
        if (root.key.equals(key)) {
            return root.value;
        }
        if (this.compareKeys((K) key, root.key) > 0) {
            return getUsingRec(key, root.right);
        } else {
            return getUsingRec(key, root.left);
        }
    }

    private Entry<K, V> getParentEntry(K key) {
        Entry<K, V> current = root;
        Entry<K, V> parent = root;
        while (!current.key.equals(key)) {
            parent = current;
            if (this.compareKeys(key, current.key) > 0) {
                current = current.right;
            } else {
                current = current.left;
            }
            if (Objects.isNull(current)) {
                return null;
            }
        }
        return parent;
    }

    @Override
    public V put(K key, V value) {
        if (Objects.isNull(key)) {
            return null;
        }
        root = putUsingRec(key, value, root);
        return value;
    }

    private Entry<K, V> putUsingRec(K key, V value, Entry<K, V> root) {
        if (Objects.isNull(root)) {
            root = new Entry<>(key, value);
            size++;
            return root;
        }
        if (root.key.equals(key)) {
            root.value = value;
            return root;
        }
        if (this.compareKeys(key, root.key) > 0) {
            root.right = putUsingRec(key, value, root.right);
        } else {
            root.left = putUsingRec(key, value, root.left);
        }
        return root;
    }

    @Override
    public V remove(Object key) {
        if (Objects.isNull(key)) {
            return null;
        }
        Entry<K, V> parent = getParentEntry((K) key);
        if (Objects.isNull(parent)) {
            return null;
        }
        Entry<K, V> current = (compareKeys((K) key, parent.key) > 0) ? parent.right : parent.left;
        if (Objects.isNull(current.left) && Objects.isNull(current.right)) {
            deleteIfHasNoChild(current, parent);
        } else if (Objects.isNull(current.left) || Objects.isNull(current.right)) {
            deleteIfHasOneChild(current, parent);
        } else {
            deleteIfHasTwoChilds(current, parent);
        }
        size--;
        return current.value;
    }

    private void deleteIfHasNoChild(Entry<K, V> node, Entry<K, V> parentNode) {
        if (node.equals(root)) {
            root = null;
        } else if (node.equals(parentNode.left)) {
            parentNode.left = null;
        } else {
            parentNode.right = null;
        }
    }

    private void deleteIfHasOneChild(Entry<K, V> node, Entry<K, V> parentNode) {
        if (Objects.isNull(node.left)) {
            if (node.equals(root)) {
                root = root.right;
            } else if (node.equals(parentNode.left)) {
                parentNode.left = node.right;
            } else {
                parentNode.right = node.right;
            }
        } else if (Objects.isNull(node.right)) {
            if (node.equals(root)) {
                root = root.left;
            } else if (node.equals(parentNode.left)) {
                parentNode.left = node.left;
            } else {
                parentNode.right = node.left;
            }
        }
    }

    private void deleteIfHasTwoChilds(Entry<K, V> node, Entry<K, V> parentNode) {
        Entry<K, V> successor = getSuccessor(node);
        if (node.equals(root)) {
            root = successor;
        } else if (node.equals(parentNode.left)) {
            parentNode.left = successor;
        } else {
            parentNode.right = successor;
        }
        //successor.left = node.left;
    }

    private Entry<K, V> getSuccessor(Entry<K, V> node) {
        Entry<K, V> successor = node;
        Entry<K, V> successorParent = node;
        Entry<K, V> current = node.right;
        while (!Objects.isNull(current)) {
            successorParent = successor;
            successor = current;
            current = current.left;
        }
        if (!successor.equals(node.right)) {
            successorParent.left = successor.right;
            successor.right = node.right;
            successor.left = node.left;
        }
        return successor;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        if (Objects.isNull(m)) {
            return;
        }
        for (Map.Entry<? extends K, ? extends V> mapEntry : m.entrySet()) {
            this.put(mapEntry.getKey(), mapEntry.getValue());
        }
    }

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keys = new LinkedHashSet<>(size);
        keySetRec(root, keys);
        return keys;
    }

    private void keySetRec(Entry<K, V> root, Set<K> keys) {
        if (Objects.isNull(root)) {
            return;
        }
        keySetRec(root.left, keys);
        keys.add(root.key);
        keySetRec(root.right, keys);
    }

    @Override
    public Collection<V> values() {
        List<V> values = new ArrayList<>(size);
        for (K key : keySet()) {
            values.add(get(key));
        }
        return values;
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> entries = new LinkedHashSet<>(size);
        entrySetRec(root, entries);
        return entries;
    }

    private void entrySetRec(Entry<K, V> root, Set<Map.Entry<K, V>> entries) {
        if (Objects.isNull(root)) {
            return;
        }
        entrySetRec(root.left, entries);
        entries.add(root);
        entrySetRec(root.right, entries);
    }

    private int compareKeys(K key1, K key2) {
        if (Objects.isNull(comparator)) {
            Comparable<K> comparable = (Comparable<K>) key1;
            return comparable.compareTo(key2);
        } else {
            return comparator.compare(key1, key2);
        }
    }
}
